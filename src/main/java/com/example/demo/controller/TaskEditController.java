package com.example.demo.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Task;
import com.example.demo.service.TaskEditService;

@Controller
public class TaskEditController {
	public static final String TASK_BLANK = "タスクを入力してください";
	public static final String LIMIT_DATE_BLANK = "期限を設定してください";
	public static final String OVER_CONTENT ="140文字以内で入力してください";
	public static final String INVALID_DATE ="無効な日付です";
	public static final String INVALID_PARAMETER ="不正なパラメータです";

	@Autowired
	TaskEditService editService;

	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") String id) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<>();
		if(id == "" && !id.matches("^[0-9]+$")) {
			errorMessages.add("INVALID_PARAMETER");

		}

		Task taskData = editService.findById(id);

		if(taskData == null) {
			errorMessages.add("INVALID_PARAMETER");
		}
		if(errorMessages.size() != 0) {
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("redirect:/");
			return mav;
		}

		String date = new SimpleDateFormat("yyyy-MM-dd").format(taskData.getLimitDate());
		mav.addObject("formModel", taskData);
		mav.addObject("date", date);
		mav.setViewName("/edit");
		return mav;

	}
	@PostMapping("/update")
	public ModelAndView updateTask(@ModelAttribute("formModel") Task task, @ModelAttribute("date") String date) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<>();
		date += new SimpleDateFormat(" HH:mm:ss").format(new Date());
		if(date.equals(new SimpleDateFormat(" HH:mm:ss").format(new Date()))) {
			errorMessages.add(LIMIT_DATE_BLANK);
		} else {
			if(toTimestamp(date).before(Timestamp.valueOf(getDate())) ) {
				errorMessages.add(INVALID_DATE);
			}
		}
		if ((!StringUtils.hasLength(task.getContent())) || (!StringUtils.hasText(task.getContent()))) {
            errorMessages.add(TASK_BLANK);
        } else if (140 < task.getContent().length()) {
            errorMessages.add(OVER_CONTENT);
        }

		if(errorMessages.size() != 0) {
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/edit");
			return mav;
		}
		editService.saveTask(task, toTimestamp(date));
		return new ModelAndView("redirect:/");
	}

	private String getDate() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

	private Timestamp toTimestamp(String date) {
		return Timestamp.valueOf(date);
	}

}
