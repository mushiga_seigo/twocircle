package com.example.demo.service;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Task;
import com.example.demo.repository.TaskRepository;

@Service
public class TaskEditService {
	@Autowired
	TaskRepository taskRepository;

	public Task findById(String id) {
		return taskRepository.findById(Integer.parseInt(id)).orElse(null);

	}

	public void saveTask(Task task, Timestamp taskLimit) {
		task.setLimitDate(taskLimit);
		taskRepository.save(task);
	}

}
