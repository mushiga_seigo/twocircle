package com.example.demo.service;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Task;
import com.example.demo.repository.TaskRepository;

@Service
public class TaskNewService {
	@Autowired
	TaskRepository taskRepository;

	// タスク追加
	public void saveTask(Task task, Timestamp limit_date) {
		task.setLimitDate(limit_date);
		taskRepository.save(task);
	}
}
